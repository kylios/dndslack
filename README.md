DnD Slack
=====

A SlackBot for Dungeons & Dragons.

# Features

## Slash commands

`/spell [spell name]`

Looks up a spell and displays its info to the Slack channel.

# Testing

You can test `dndslack` in several ways.

To start a local dev server, just run

```bash
npm run start
```

This server will start listening on port 3000. This is useful if you're savvy
with curl. However, you'll probably want to imitate Slack when testing.

It's possible to serve your local server to the world-wide-web using `ngrok`.
First download and install the tool. Instructions vary by operating system, so
first read up on their [website](https://ngrok.com/download).

To open your server up to the web, run:

```bash
ngrok http 3000
```

You should see output that looks like this:

```
ngrok by @inconshreveable                                                 (Ctrl+C to quit)

Session Status                online                                                      
Session Expires               7 hours, 59 minutes                                         
Update                        update available (version 2.2.9, Ctrl-U to update)          
Version                       2.2.9                                                       
Region                        United States (us)                                          
Web Interface                 http://127.0.0.1:4040                                       
Forwarding                    http://cefc4a42.ngrok.io -> localhost:3000                  
Forwarding                    https://cefc4a42.ngrok.io -> localhost:3000                 

Connections                   ttl     opn     rt1     rt5     p50     p90                 
                              0       0       0.00    0.00    0.00    0.00       
```

Now, any web request made to `http://cefc4a42.ngrok.io` or the `https`
equivalent will be forwarded to your local server. Create a Slack application in
your workspace and connect it to this URL.

You can also monitor all incoming requests using the web interface at
`http://127.0.0.1:4040`. Super handy.

# Deploying

I'm trying to use [zeit's](https://zeit.co) serverless product. This allows us
to ship a docker image to the cloud and have them host it for us. Super nifty.

When you're ready to deploy, run:

```bash
./node_modules/now/download/dist/now --public
```

Press `2` at the prompt to use the `Dockerfile`. When it's all done, the app
should be hosted at the provided URL.
