const express = require('express')
const bodyParser = require('body-parser')
const spells = require('./spells.js')

const PORT = 3000

function indexify(spellText) {
    return spellText
        .toLowerCase()
        .replace(/ /g, '_')
        .replace('’', '')
        .replace("'", '')
}

function namify(spellName) {
    return spellName
        .split('_')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')
}

const app = express()
app.use(bodyParser.urlencoded())

app.post('/command', function(req, res) {
  console.log('POST /command')

  const spellName = indexify(req.body.text)
  console.log(`Spell: ${spellName}`)

  const spell = spells.lookupSpell(spellName)

  if (!spell) {
    res.send(`Couldn't find the spell _${namify(spellName)}_!`)
    return
  }

  const response = {
    attachments: [
      {
        title: namify(spellName),
        text: `_${spell.description}_`,
      },
      {
        title: 'Details',
        fields: [
          {
            title: 'Range',
            value: spell.range,
            short: true
          },
          {
            title: 'Casting Time',
            value: spell.casting_time,
            short: true
          },
          {
            title: 'Level',
            value: spell.level,
            short: true
          },
          {
            title: 'Components',
            value: spell.components,
            short: true
          },
          {
            title: 'Duration',
            value: spell.duration,
            short: true
          },
          {
            title: 'School',
            value: spell.school,
            short: true
          }
        ]
      }
    ]
  }

  res.json(response)
})

app.get('/', function (req, res) {
    res.send('hello')
})

app.listen(PORT, () => console.log(`Listening on port ${PORT}`))
