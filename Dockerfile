FROM node:10-alpine
MAINTAINER Kyle Racette "kracette@gmail.com"

EXPOSE 3000
USER root

ADD --chown=root:root package.json /app/
ADD --chown=root:root package-lock.json /app/

RUN cd /app && npm install

ADD --chown=root:root spells.json /app/

ADD --chown=root:root run_app.sh /
RUN chmod +x /run_app.sh

ADD --chown=root:root *.js /app/

ENTRYPOINT ["/run_app.sh"]
