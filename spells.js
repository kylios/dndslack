const fs = require('fs')

function loadSpells() {
    console.log("Loading spells database...")
    return JSON.parse(fs.readFileSync('./spells.json'))
}

const SPELLS = loadSpells()

function lookupSpell(spellName) {
    console.log(`Looking up "${spellName}"`)

    return SPELLS[spellName]
}

function getAllSpells() {
    return SPELLS
}

module.exports = { getAllSpells, lookupSpell }
